# frozen_string_literal: true

class Sign < ApplicationRecord
  belongs_to :document
  validates :document, presence: true
  validates :latitude, presence: true
  validates :longitude, presence: true

  validate :uniq_coordinates

  private

  def uniq_coordinates
    errors.add('Объект', 'присутствует по данным координатам') if Sign.exists?(latitude: latitude, longitude: longitude) && Sign.find_by(latitude: latitude, longitude: longitude) != self
  end
end
