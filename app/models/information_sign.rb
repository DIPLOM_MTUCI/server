# frozen_string_literal: true

class InformationSign < ApplicationRecord
  before_create :default_height

  private

  def default_height
    self.height ||= 180
  end
end
