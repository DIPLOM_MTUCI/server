# frozen_string_literal: true

class Document < ApplicationRecord
  has_one :step
  has_one :sign
end
