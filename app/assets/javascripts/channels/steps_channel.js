App.cable.subscriptions.create({ channel: "StepsChannel" }, {
  received(data) {
    console.log(data);
    if (document.getElementsByTagName("p")[3] == null && document.getElementsByTagName("p")[3].innerText != ("Идентификатор: " + data.identifier)) { return; }

    this.changeData(data)
  },

  changeData(data) {
    document.getElementsByTagName("img")[0].src=data.image_from_object
    document.getElementsByTagName("img")[1].src=data.detected_image_from_object
    document.getElementsByTagName("p")[4].innerHTML = ("<b>Долгота:</b>" + data.gps.longitude)
    document.getElementsByTagName("p")[5].innerHTML = ("<b>Широта:</b>" + data.gps.latitude)

    if(data.signs != null)
      {
        
        document.getElementsByTagName("p")[6].innerHTML = "<b>Ближайшие статические объекты:</b><a class=\"pure-button pure-button-primary\" name=\"image_from_object\" href=\""+data.signs[1]+"\">Первый статичесикий объект</a>\
          <a class=\"pure-button pure-button-primary\" name=\"detected_image\" href=\""+data.signs[0]+"\">Второй статический объект</a>"
      }
    else
      {
        document.getElementsByTagName("p")[6].innerHTML = "<b>Ближайшие статические объекты:</b> Статических объектов по близости не найдено"
      }
    if(data.find_objects != null)
      {
        document.getElementsByTagName("p")[12].innerHTML = data.find_objects
      }
    else
      {
        document.getElementsByTagName("p")[12].innerHTML = "Объекты не найдены"
      } 

  }
})

