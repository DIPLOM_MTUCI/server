# frozen_string_literal: true

class DecreaseErrorGps
  attr_reader :two_near_signs, :detected_objects, :detected_signs, :gps_object, :two_near_detected_signs

  def initialize(near_signs:, detected_objects:, gps_object:)
    @two_near_signs = near_signs
    @detected_objects = detected_objects
    @gps_object = gps_object
  end

  def call
    return gps_object.merge(correct: 'Нет в базе статических объектов по близости') if two_near_signs.blank?

    @two_near_detected_signs = getting_two_near_detected_signs
    decrease_error_gps
  end

  private

  def decrease_error_gps
    d = Geokit::LatLng.new(two_near_signs.first.latitude, two_near_signs.first.longitude).distance_to(Geokit::LatLng.new(two_near_signs.last.latitude, two_near_signs.last.longitude))
    d = 64 if Rails.env.test?
    r_1 = two_near_detected_signs[two_near_signs.first.name.to_sym][:distance]
    r_2 = two_near_detected_signs[two_near_signs.last.name.to_sym][:distance]
    # Если не корректные данные
    return gps_object.merge(correct: 'Не получилось вычислить расстояние до объекта') if r_1.is_a?(String) || r_2.is_a?(String)
    return gps_object.merge(correct: 'Одна коружность содежрится в другой') if d < (r_2 - r_1).abs
    return gps_object.merge(correct: 'Окружности не пересекаются') if d > r_2 + r_1

    a = (r_1**2 - r_2**2 + d**2) / (2 * d)
    h = Math.sqrt(r_1**2 - a**2)
    p_x = two_near_signs.first.longitude + a * (two_near_signs.last.longitude - two_near_signs.first.longitude) / d
    p_y = two_near_signs.first.latitude + a * (two_near_signs.last.latitude - two_near_signs.first.latitude) / d

    crossing_x_1 = p_x + h * (two_near_signs.last.longitude - two_near_signs.first.longitude) / d
    crossing_y_1 = p_y - h * (two_near_signs.last.latitude - two_near_signs.first.latitude) / d

    crossing_x_2 = p_x - h * (two_near_signs.last.longitude - two_near_signs.first.longitude) / d
    crossing_y_2 = p_y + h * (two_near_signs.last.latitude - two_near_signs.first.latitude) / d

    first_dist = Geokit::LatLng.new(crossing_y_1, crossing_x_1).distance_to(Geokit::LatLng.new(gps_object[:latitude], gps_object[:longitude]))
    sec_dist = Geokit::LatLng.new(crossing_y_2, crossing_x_2).distance_to(Geokit::LatLng.new(gps_object[:latitude], gps_object[:longitude]))

    first_dist < sec_dist ? { latitude: crossing_y_1, longitude: crossing_x_1, correct: true } : { latitude: crossing_y_2, longitude: crossing_x_2, correct: true }
  end

  def getting_two_near_detected_signs
    # Приводим к виду {static_object: [{:persent_detect, :left_x, :top_y, :width, :height, :distance}]}
    @detected_signs = detected_objects.each_with_object({}) do |arr, hash|
      hash[arr[:object]] = [] if hash[arr[:object]].nil?
      hash[arr[:object]] << arr.except(:object)
    end
    # Фильтруем найденые объекты с камеры убираем не нужные
    fitlered_detected_object = detected_signs.select do |k, _v|
      two_near_signs.map(&:name).include? k
    end
    # Если нет объектов, значит мы не смогли определить их
    return {} unless fitlered_detected_object.keys.count == 2

    # Сортируем по расстоянию до статических объектов
    sorted_detected_signs = fitlered_detected_object.each_with_object({}) { |(k, v), hash| hash[k.to_sym] = v.sort_by { |value| value[:distance] } }
    # Приводим к первоночальному виду и выбираем объекты которые two_near_signs
    sorted_detected_signs.each_with_object({}) { |(k, v), hash| hash[k.to_sym] = v.first }.select { |sign| two_near_signs.map(&:name).include? sign.to_s }
  end
end
