# frozen_string_literal: true

class GetDetectedImage
  attr_reader :document

  def initialize(document)
    @document = document
  end

  def call
    run_yolo
  end

  private

  def run_yolo
    # TODO: добавить мьютекс
    File.open('tmp/img.jpg', 'wb') { |f| f.write document[:tempfile] }
    command = 'cd .. && darknet detect server/storage/yolo/yolo.cfg server/storage/yolo/yolo.weights server/tmp/img.jpg -ext_output && cp predictions.jpg server/tmp/result.jpg'
    detected = Kernel.send(:`, command)
    result = detected.split("\n")[9..detected.split("\n").count - 1].each_with_object([]) do |str, arr|
      parse = str.match(/^([\w+\s+]+):\s+([\d+-]+)%\t\((\w+):\s+([\d+-]+)\s+(\w+):\s+([\d+-]+)\s+(\w+):\s+([\d+-]+)\s+(\w+):\s+([\d+-]+)\)$/)
      arr << (parse.nil? ? 'Не корректная регулярка, для данного объекта' : { object: parse[1], persent_detect: parse[2], left_x: parse[4], top_y: parse[6], width: parse[8], height: parse[10] })
    end
    [Rack::Test::UploadedFile.new('tmp/result.jpg', 'image/jpg').read, result.presence]
  end
end
