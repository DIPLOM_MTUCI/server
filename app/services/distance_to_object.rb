# frozen_string_literal: true
class DistanceToObject
  class ErrorDistanceToObject < StandardError; end
  attr_reader :h_pix, :all_pix_h, :object

  REALY_H = (ENV['REALY_SIZE_OBJECT'].presence.nil? ? 1.80 : ENV['REALY_SIZE_OBJECT'].to_f) # средний рост человека
  F = ENV['FOCUS_DISTANCE'].presence.nil? ? 0.13 : ENV['FOCUS_DISTANCE'].to_f # стандарт 22–26 мм у смартфонов
  MATRIX_H = ENV['MATIX_HIGHT'].presence.nil? ? 7.57 : ENV['MATIX_HIGHT'].to_f # iphone 11 ширина смартфона

  def initialize(h_pix, all_pix_h, object:)
    @h_pix = h_pix
    @all_pix_h = all_pix_h
    @object = object
  end

  def call
    distance
  end

  private

  def distance
    hight_at_matrix = (h_pix.to_f * MATRIX_H / all_pix_h.to_f) / 100
    F * (hight_at_matrix + really_h) / hight_at_matrix
    # really_h - размер объекта в реальности в метрах
    # MATRIX_H - высоты матрицы девайся(физическая) в сантиметрах
    # hight_at_matrix - размер объекта в матрице в метрах
    # F - фокусное расстояние объектива
  rescue
    'Параметры телефона указаны не верно для определения растояния'
  end

  def really_h
    inf = InformationSign.find_by(name: object.to_s)
    return REALY_H if inf.nil?

    # В сантиментрах
    inf.height / 100
  end
end
