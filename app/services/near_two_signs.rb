# frozen_string_literal: true

class NearTwoSigns
  attr_reader :step_latitude, :step_longitude

  METERS = Integer(ENV['BINDING_BOX_METERS']) # размер квадрата выбора ближайших знаков
  DELTA_GPS = METERS * 0.00000898

  def initialize(identifier)
    step = Hbase::GetService.new(identifier).call
    raise ActiveRecord::RecordNotFound, 'Не существует ходов с данным идентификатором' if step.blank?

    @step_latitude = step[:latitude]
    @step_longitude = step[:longitude]
  end

  def call
    near_signs_from_binding_box
  end

  private

  def near_signs_from_binding_box
    step_gps = Geokit::LatLng.new(step_latitude, step_longitude)
    min_first_sign = nil
    min_sec_sign = nil
    min_first = METERS + 100
    min_sec = METERS + 100
    # Выбираем в квадрате +-DELTA_GPS
    range_longitude = [step_gps.longitude - DELTA_GPS..step_gps.longitude + DELTA_GPS]
    range_latitude = [step_gps.latitude - DELTA_GPS..step_gps.latitude + DELTA_GPS]
    Sign.where(longitude: range_longitude, latitude: range_latitude).each do |sign|
      sign_gps = Geokit::LatLng.new(sign.latitude, sign.longitude)
      l = step_gps.distance_to(sign_gps)
      next unless l < min_first || min_sec == METERS + 100

      min_sec = min_first
      min_first = l
      min_sec_sign = min_first_sign
      min_first_sign = sign
    end

    return nil if min_first_sign.nil? || min_sec_sign.nil?

    [min_first_sign, min_sec_sign]
  end
end
