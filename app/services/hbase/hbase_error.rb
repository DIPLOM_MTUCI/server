# frozen_string_literal: true

module Hbase
  class HbaseError < StandardError; end
end
