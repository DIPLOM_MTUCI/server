# frozen_string_literal: true

module Hbase
  class GetService
    attr_reader :key

    def initialize(key = nil)
      @key = key
    end

    def call
      return nil if key.nil?

      geting_from_redis
    end

    private

    def geting_from_redis
      data = JSON.parse(REDIS.get(key)).deep_symbolize_keys
      data[:document] = decoding_document(data)

      data.merge(identifier: key)
    rescue
      nil
    end

    def decoding_document(data)
      data[:document].merge(tempfile: data[:document][:tempfile].force_encoding('UTF-8').encode('ISO-8859-1'))
    end
  end
end
