# frozen_string_literal: true

module Hbase
  class SetService
    attr_reader :key, :data

    def initialize(key = nil, data = nil)
      @key = key
      @data = data.deep_symbolize_keys
    end

    def call
      raise Redis::RedisError, 'Нельзя добавить в бд redis с пустым ключем' if key.nil?

      set_to_redis
    end

    private

    def set_to_redis
      data[:document] = encoding_document

      REDIS.set key, data.to_json
    rescue => e
      raise HbaseError, "Ошибка добавления данных в бд redis: #{e.message}"
    end

    def encoding_document
      data[:document].merge(tempfile: data[:document][:tempfile].force_encoding('ISO-8859-1').encode('UTF-8'))
    end
  end
end
