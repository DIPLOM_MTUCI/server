# frozen_string_literal: true

class StepsChannel < ApplicationCable::Channel
  def subscribed
    stream_from 'steps_channel'
  end
end
