# frozen_string_literal: true

json.extract! step, :id, :created_at, :updated_at
json.url step_url(step, format: :json)
