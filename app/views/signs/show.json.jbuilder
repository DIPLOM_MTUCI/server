# frozen_string_literal: true

json.partial! 'signs/sign', sign: @sign
