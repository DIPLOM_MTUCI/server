# frozen_string_literal: true

json.array! @signs, partial: 'signs/sign', as: :sign
