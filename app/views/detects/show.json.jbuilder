# frozen_string_literal: true

json.partial! 'detects/detect', detect: @detect
