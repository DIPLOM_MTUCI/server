# frozen_string_literal: true

module API
  module V1
    class StepApi < API::V1::Base
      resource :step do
        desc 'GPS и фото с устройства'
        params do
          requires :identifier, type: String, desc: 'Идентификатор устройства (MAC)'
          requires :image, type: File, desc: 'Изображение'
          requires :gps, type: Hash do
            requires :latitude, type: Float, desc: 'GPS широта'
            requires :longitude, type: Float, desc: 'GPS долгота'
          end
        end
        post do
          size = FastImage.size(declaring_params[:image][:tempfile])
          image = declaring_params[:image][:tempfile].read
          Hbase::SetService.new(declaring_params[:identifier], declaring_params[:gps].slice(:latitude, :longitude)
                                                               .merge(document: { content_type: declaring_params[:image][:type],
                                                                                  tempfile: image })).call
          detected_image = GetDetectedImage.new(tempfile: image).call

          detected_image[1] = detected_image[1].each_with_object([]) { |hash, arr| arr << hash.merge(distance: DistanceToObject.new(hash[:height].to_i, size.last, object: hash[:object]).call) }
          near_signs = NearTwoSigns.new(declaring_params[:identifier]).call
          ActionCable.server.broadcast 'steps_channel', identifier: declaring_params[:identifier],
                                                        image_from_object: "data:image/jpeg;base64,#{Base64.encode64(image).gsub("\n", '')}",
                                                        detected_image_from_object: "data:image/jpeg;base64,#{Base64.encode64(detected_image.first).gsub("\n", '')}",
                                                        gps: { latitude: declaring_params[:gps][:latitude], longitude: declaring_params[:gps][:longitude] },
                                                        signs: near_signs.nil? ? nil : ["/signs/#{near_signs.first.id}", "/signs/#{near_signs.last.id}"],
                                                        find_objects: detected_image.last.to_json

          DecreaseErrorGps.new(near_signs: near_signs, detected_objects: detected_image.last, gps_object: declaring_params[:gps]).call
        end

        desc 'Получить список идентификаторов устройств'
        get do
          { identifiers: REDIS.keys }
        end

        route_param :identifier do
          desc 'Получить последний ход устройства'
          get do
            last_step = Hbase::GetService.new(params[:identifier]).call
            if last_step.blank?
              raise ActiveRecord::RecordNotFound,
                    'Не существует ходов с данным идентификатором'
            end
            last_step.slice(:latitude, :longitude)
          end
        end
      end
    end
  end
end
