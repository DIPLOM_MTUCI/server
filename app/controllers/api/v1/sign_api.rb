# frozen_string_literal: true

module API
  module V1
    class SignApi < API::V1::Base
      resource :signs do
        route_param :identifier do
          desc 'Получить ближайшие два знака по последнему шагу'
          get do
            present NearTwoSigns.new(params[:identifier]).call
          end
        end
      end
    end
  end
end
