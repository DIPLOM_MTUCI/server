# frozen_string_literal: true

module API
  module V1
    class ImageApi < API::V1::Base
      resource :image do
        desc 'Получить последнее переданное изображение'
        get do
          # header 'Content-Disposition', "attachment;"
          header 'filename="last_step"'
          header 'Content-Type', 'image/jpeg'
          env['api.format'] = :binary
          command = 'cd .. && darknet detect server/storage/yolo/yolo.cfg server/storage/yolo/yolo.weights server/tmp/image/znk.jpg -ext_output && cp predictions.jpg server/tmp/image/predictions.jpg'
          Kernel.send(:`, command)
          File.open('tmp/image/predictions.jpg').read
          # Document.last.tempfile
        end

        desc 'Получить последнее переданное изображение c идентификатором'
        route_param :identifier do
          get do
            # header 'Content-Disposition', "attachment;"
            header 'filename="last_step"'
            header 'Content-Type', 'image/jpeg'
            env['api.format'] = :binary

            last_step = Hbase::GetService.new(params[:identifier]).call
            if last_step.blank?
              raise ActiveRecord::RecordNotFound,
                    'Не существует ходов с данным идентификатором'
            end

            last_step[:document][:tempfile]
          end
        end
      end
    end
  end
end
