# frozen_string_literal: true

module API
  module V1
    class Base < Grape::API
      version 'v1', using: :path
      format :json
      helpers do
        def declaring_params
          declared(params, include_missing: false)
        end
      end

      rescue_from ActiveRecord::RecordNotFound do |e|
        errors = [{ title: e.message }]
        error!({ errors: errors }, 404)
      end

      mount API::V1::ImageApi
      mount API::V1::StepApi
      mount API::V1::SignApi
    end
  end
end
