# frozen_string_literal: true

module API
  module Entities
    class Image < API::Entities::Base
      expose :image, documentation: { type: File, desc: 'Файл', required: true }
    end
  end
end
