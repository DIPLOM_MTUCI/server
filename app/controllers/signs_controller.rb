# frozen_string_literal: true

class SignsController < ApplicationController
  layout 'signs/main_signs'
  before_action :set_sign, only: %i[show edit update destroy]

  # GET /signs or /signs.json
  def index
    @signs = Sign.all.order(:id).reverse
  end

  # GET /signs/1 or /signs/1.json
  def show; end

  # GET /signs/new
  def new
    @sign = Sign.new
  end

  # GET /signs/1/edit
  def edit; end

  # POST /signs or /signs.json
  def create
    document = Document.create!(filename: sign_image.original_filename,
                                content_type: sign_image.content_type,
                                head: sign_image.headers,
                                tempfile: sign_image.tempfile.read)
    @sign = Sign.new(sign_params.as_json.merge(document_id: document.id))

    respond_to do |format|
      if @sign.save
        format.html { redirect_to @sign, notice: 'Статический объект успешно создан.' }
        format.json { render :show, status: :created, location: @sign }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @sign.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /signs/1 or /signs/1.json
  def update
    if params[:image].present?
      @sign.document.update!(filename: sign_image.original_filename,
                             content_type: sign_image.content_type,
                             head: sign_image.headers,
                             tempfile: sign_image.tempfile.read)
    end

    respond_to do |format|
      if @sign.update(sign_params.as_json)
        format.html { redirect_to @sign, notice: 'Статический объект успешно обновлен.' }
        format.json { render :show, status: :ok, location: @sign }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @sign.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /signs/1 or /signs/1.json
  def destroy
    @sign.destroy
    respond_to do |format|
      format.html { redirect_to signs_url, notice: 'Статический объект успешно удален.' }
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_sign
    @sign = Sign.find(params[:id])
  end

  # Only allow a list of trusted parameters through.
  def sign_params
    params.require(:sign).permit(:longitude, :latitude, :name, :desc)
  end

  def sign_image
    params.require(:sign).fetch(:image)
  end
end
