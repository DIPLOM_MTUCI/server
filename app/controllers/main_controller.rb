# frozen_string_literal: true

class MainController < ApplicationController
  layout 'main/main_main'
  def show; end
end
