# frozen_string_literal: true

class DetectsController < ApplicationController
  layout 'detects/detect_main'
  # GET /steps or /steps.json
  def new; end

  def show
    detected = GetDetectedImage.new(tempfile: params.require(:detect).fetch(:image).read).call
    @detected_image = detected.first
    return if detected.last.nil?

    size = FastImage.size(params.require(:detect).fetch(:image).tempfile)
    @detected_info = detected.last.each_with_object([]) { |hash, arr| arr << hash.merge(distance: DistanceToObject.new(hash[:height].to_i, size.last, object: hash[:object]).call) }
  end
end
