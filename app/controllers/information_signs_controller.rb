# frozen_string_literal: true

class InformationSignsController < ApplicationController
  layout 'information_signs/main_information_signs'
  before_action :set_information_sign, only: %i[show edit update destroy]

  # GET /information_signs or /information_signs.json
  def index
    @information_signs = InformationSign.all.order(:id).reverse
  end

  # GET /signs/1 or /signs/1.json
  def show; end

  # GET /signs/new
  def new
    @information_sign = InformationSign.new
  end

  # GET /signs/1/edit
  def edit; end

  # POST /signs or /signs.json
  def create
    @information_sign = InformationSign.new(information_sign_params.as_json)

    respond_to do |format|
      if @information_sign.save
        format.html { redirect_to @information_sign, notice: 'Информация о статическом объекте успешно созданa.' }
        format.json { render :show, status: :created, location: @information_sign }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @information_sign.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /signs/1 or /signs/1.json
  def update
    respond_to do |format|
      if @information_sign.update(information_sign_params.as_json)
        format.html { redirect_to @information_sign, notice: 'Информация о статическом объекте успешно обновлена.' }
        format.json { render :show, status: :ok, location: @information_sign }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @information_sign.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /signs/1 or /signs/1.json
  def destroy
    @information_sign.destroy
    respond_to do |format|
      format.html { redirect_to information_signs_url, notice: 'Информация о статическом объекте успешно удалена.' }
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_information_sign
    @information_sign = InformationSign.find(params[:id])
  end

  # Only allow a list of trusted parameters through.
  def information_sign_params
    params.require(:information_sign).permit(:name, :height)
  end
end
