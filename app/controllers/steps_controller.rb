# frozen_string_literal: true

class StepsController < ApplicationController
  layout 'steps/main_steps'
  before_action :set_step, only: %i[show edit update destroy]

  # GET /steps or /steps.json
  def index
    @steps = REDIS.keys
  end

  # GET /steps/1 or /steps/1.json
  def show
    detected = GetDetectedImage.new(tempfile: @step[:document][:tempfile]).call
    @detected_image = detected.first
    return if detected.last.nil?

    size = Tempfile.open(['img', '.jpg']) do |f|
      f.binmode
      f.write @step[:document][:tempfile]
      f.rewind
      FastImage.size(f)
    end
    @detected_info = detected.last.each_with_object([]) { |hash, arr| arr << hash.merge(distance: DistanceToObject.new(hash[:height].to_i, size.last, object: hash[:object]).call) }
  end

  # GET /steps/1/edit
  def edit; end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_step
    @step = Hbase::GetService.new(params[:id]).call
  end

  # Only allow a list of trusted parameters through.
  def step_params
    params.fetch(:step, {})
  end
end
