create_array = [
  {name: 'person', height: 180.0},
  {name: 'truck', height: 230.0},
  {name: 'car', height: 160.0}
]

create_array.each do |inf|
  InformationSign.find_or_create_by(inf)
end