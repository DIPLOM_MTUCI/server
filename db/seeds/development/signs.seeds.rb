after 'development:documents' do
signs_list = [
    {longitude: 37.59283304214478, latitude: 55.76262565355965, name: 'левый знак', document_id:Document.find_by(filename: 'first_sign').id},
    {longitude: 37.59305834770203, latitude: 55.762676961969106, name: 'правый знак', document_id:Document.find_by(filename: 'second_sign').id}
]

signs_list.each do |sign|
  Sign.find_or_create_by(sign.except(:name, :desc)).update(sign.slice(:name, :desc))
end
end