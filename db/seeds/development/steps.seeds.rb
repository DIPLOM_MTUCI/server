after 'development:documents' do
  step_list = [ {identifier:'mac_1', longitude: 37.59283304214478, latitude: 55.76262565355965, document_id: Document.find_by(filename: 'first_step').id},
                {identifier:'mac_2', longitude: 37.592833042132, latitude: 55.76262565323, document_id: Document.find_by(filename: 'second_step').id}
              ]
step_list.each do |step|
    document = Document.find(step[:document_id]).as_json
    Hbase::SetService.new(step[:identifier], step.except(:document_id).merge(document: document)).call
  end
end