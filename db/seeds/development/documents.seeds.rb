
document_list = [
  {filename:'first_sign', content_type:'image/jpg', head:'', tempfile: Rack::Test::UploadedFile.new('spec/fixtures/files/sign1.jpg', 'image/jpg').read},
  {filename:'second_sign', content_type:'image/jpg', head:'', tempfile: Rack::Test::UploadedFile.new('spec/fixtures/files/sign2.jpg', 'image/jpg').read},
  {filename:'first_step', content_type:'image/jpg', head:'', tempfile: Rack::Test::UploadedFile.new('spec/fixtures/files/sign1.jpg', 'image/jpg').read},
    {filename:'second_step', content_type:'image/jpg', head:'', tempfile: Rack::Test::UploadedFile.new('spec/fixtures/files/sign2.jpg', 'image/jpg').read}
]

document_list.each do |document|
  Document.find_or_create_by!(document)
end