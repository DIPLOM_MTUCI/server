# frozen_string_literal: true

class CreateTables < ActiveRecord::Migration[5.2]
  def change
    create_table :documents do |t|
      t.string :filename
      t.string :content_type
      t.string :head
      t.binary :tempfile

      t.timestamps
    end

    # create_table :steps do |t|
    #   t.string :identifier, index: true
    #   t.float :latitude
    #   t.float :longitude
    #   t.belongs_to :document, foreign_key: true

    #   t.timestamps
    # end

    create_table :information_signs do |t|
      t.string :name, unique: true
      t.float :height

      t.timestamps
    end

    create_table :signs do |t|
      t.float :latitude
      t.float :longitude
      t.string :name
      t.string :desc
      t.belongs_to :document, foreign_key: true

      t.timestamps
    end
  end
end
