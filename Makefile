bash:
	docker-compose run --rm web_gps bash

init:
	docker-compose run --rm web_gps rake db:create db:migrate db:seed:development
	docker-compose run -e RAILS_ENV=test --rm web_gps rake db:create db:migrate

up:
	docker-compose up -d
	docker attach server_web_gps_1

update_init:
	docker-compose run --rm web_gps rake db:drop db:create db:migrate db:seed:development
	docker-compose run -e RAILS_ENV=test --rm web_gps rake db:drop db:create db:migrate

rspec:
	docker-compose run -e RAILS_ENV=test --rm web_gps rspec

rubocop:
	docker-compose run --rm web_gps rubocop  -P -E  .

attach:
	docker attach server_web_gps_1