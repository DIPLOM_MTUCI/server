FROM denisrulev/docker_yolo_ruby_251:latest

# Проблемы с mime-magic изза смена лицензии с GPL на MIT
RUN apt install -y p7zip-full && \
    mkdir -p mkdir /usr/share/mime/packages && cd /usr/share/mime/packages &&\
    curl http://ftp.de.debian.org/debian/pool/main/s/shared-mime-info/shared-mime-info_2.0-1_amd64.deb --output shared-mime.deb && \
    7z x -so shared-mime.deb data.tar | 7z e -sidata.tar "./usr/share/mime/packages/freedesktop.org.xml"
    
RUN apt-get install -y libexif-dev
RUN mkdir /server
WORKDIR /server

COPY Gemfile* /server/
RUN bundle install

COPY . /server
RUN cd /server && chown -R 777 .