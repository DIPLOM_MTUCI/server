# frozen_string_literal: true

Rails.application.routes.draw do
  resources :signs
  resources :steps
  resources :information_signs
  post 'detects', to: 'detects#show'
  get 'detects', to: 'detects#new'
  get 'main', to: 'main#show'
  root 'main#show'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  mount API::Base => '/api'
  mount GrapeSwaggerRails::Engine => '/swagger'
end
