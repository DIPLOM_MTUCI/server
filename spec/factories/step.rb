# frozen_string_literal: true

FactoryBot.define do
  factory :step do
    skip_create
    identifier { Faker::Internet.unique.mac_address }
    latitude { Faker::Address.latitude }
    longitude { Faker::Address.longitude }
    association :document, factory: :document

    initialize_with do
      Hbase::SetService.new(attributes.fetch(:identifier), latitude: attributes.fetch(:latitude),
                                                           longitude: attributes.fetch(:longitude),
                                                           document: attributes.fetch(:document).as_json(except: :id)).call
      Hbase::GetService.new(attributes.fetch(:identifier)).call
    end
  end
end
