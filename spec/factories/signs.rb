# frozen_string_literal: true

FactoryBot.define do
  factory :sign do
    name { Faker::Lorem.word }
    latitude { Faker::Address.unique.latitude }
    longitude { Faker::Address.unique.longitude }
    desc { Faker::Lorem.sentence }
    association :document, factory: :document
  end
end
