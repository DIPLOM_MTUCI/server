# frozen_string_literal: true

FactoryBot.define do
  factory :document do
    content_type { 'image/jpeg' }
    tempfile { Rack::Test::UploadedFile.new('spec/fixtures/files/znk.jpg', 'image/jpg').read }
    filename { Faker::Lorem.word }
    head { Faker::Lorem.word }
  end
end
