# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Sign, type: :model do
  let(:document) { create(:document) }
  let(:params) do
    { latitude: Faker::Address.unique.latitude,
      longitude: Faker::Address.unique.latitude,
      name: Faker::Lorem.word,
      desc: Faker::Lorem.sentence }
  end

  it 'Нельзя создать статический объект без изображения' do
    expect(Sign.new(params)).not_to be_valid
  end
  context 'Нельзя создать статический объект с существующими коордитанами' do
    let!(:created_sign) { create(:sign, latitude: params[:latitude], longitude: params[:longitude]) }
    it 'Валидация не верная' do
      expect(Sign.new(params)).not_to be_valid
    end
  end

  it 'Успешная валидация статического объекта' do
    expect(Sign.new(params.merge(document_id: document.id))).to be_valid
  end
end
