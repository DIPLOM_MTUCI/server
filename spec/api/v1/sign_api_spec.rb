# frozen_string_literal: true
require 'rails_helper'

describe 'GET /api/v1/signs/:identifier' do
  before do
    allow(ENV).to receive(:[]).and_call_original
    allow(ENV).to receive(:[]).with('BINDING_BOX_METERS').and_return('300')
  end
  context 'Получаем два ближайших статических объекта' do
    let!(:step) { create(:step) }
    context 'Имеются два ближайших знака в границах квадрата' do
      let!(:sign_1) { create(:sign, longitude: step[:longitude] - 0.00032, latitude: step[:latitude] - 0.0003) }
      let!(:sign_2) { create(:sign, longitude: step[:longitude] - 0.00042, latitude: step[:latitude] - 0.0004) }
      # TODO: Подключить очистку бд
      it 'Успешный запрос' do
        get "/api/v1/signs/#{step[:identifier]}"
        expect(response.status).to eq(200)
        expect(JSON.parse(response.body).count).to eq(2)
      end

      context 'Есть невошедшие в границы квадрата' do
        let!(:sign_3) { create(:sign, longitude: step[:longitude] - 0.012, latitude: step[:latitude] - 0.0004) }
        it 'Успешный запрос' do
          get "/api/v1/signs/#{step[:identifier]}"
          expect(response.status).to eq(200)
          expect(JSON.parse(response.body).count).to eq(2)
        end
      end

      context 'Есть вошедшие в границы квадрата' do
        let!(:sign_3) { create(:sign, longitude: step[:longitude] - 0.00058, latitude: step[:latitude] - 0.0005) }
        it 'Успешный запрос' do
          get "/api/v1/signs/#{step[:identifier]}"
          expect(response.status).to eq(200)
          expect(JSON.parse(response.body).count).to eq(2)
        end
      end
    end

    context 'В границе нет ближайших знаков' do
      context 'Ни одного' do
        xit 'Успешный запрос' do
          get "/api/v1/signs/#{step[:identifier]}"
          expect(response.status).to eq(404)
        end
      end

      context 'Один' do
        let!(:sign_1) { create(:sign, longitude: step[:longitude] - 0.00032, latitude: step[:latitude] - 0.0003) }
        xit 'Успешный запрос' do
          get "/api/v1/signs/#{step[:identifier]}"
          expect(response.status).to eq(404)
        end
      end
    end
  end
end
