# frozen_string_literal: true
require 'rails_helper'

describe 'POST /api/v1/step' do
  before do
    allow(ENV).to receive(:[]).and_call_original
    allow(ENV).to receive(:[]).with('FOCUS_DISTANCE').and_return('0.13')
    allow(ENV).to receive(:[]).with('MATIX_HIGHT').and_return('7.57')
    allow(ENV).to receive(:[]).with('REALY_SIZE_OBJECT').and_return('1.80')
    allow_any_instance_of(GetDetectedImage).to receive(:call).and_return([fixture_file_upload('files/znk.jpg', 'image/jpg').read,
                                                                          [{ object: 'person', persent_detect: '52', left_x: '6', top_y: '333', width: '261', height: '488' },
                                                                           { object: 'person', persent_detect: '96', left_x: '282', top_y: '345', width: '302', height: '522' },
                                                                           { object: 'truck', persent_detect: '60', left_x: '594', top_y: '237', width: '58', height: '40' },
                                                                           { object: 'person', persent_detect: '96', left_x: '663', top_y: '365', width: '369', height: '483' }]])
  end

  context 'Получаем два ближайших статических объекта' do
    let!(:params) do
      {
        identifier: Faker::Internet.unique.mac_address,
        gps: { latitude: Faker::Address.latitude, longitude: Faker::Address.longitude },
        image: fixture_file_upload('files/znk.jpg', 'image/jpg')
      }
    end
    context 'Имеются два ближайших знака в границах квадрата' do
      let!(:sign_1) { create(:sign, longitude: params[:gps][:longitude] - 0.00032, latitude: params[:gps][:latitude] - 0.0003, name: 'person') }
      let!(:sign_2) { create(:sign, longitude: params[:gps][:longitude] - 0.00042, latitude: params[:gps][:latitude] - 0.0004, name: 'truck') }
      # TODO: Подключить очистку бд
      it 'Успешный запрос' do
        post '/api/v1/step', params: params
        expect(response.status).to eq(201)
        expect(JSON.parse(response.body)).to be_truthy
      end

      context 'Есть невошедшие в границы квадрата' do
        let!(:sign_3) { create(:sign, longitude: params[:gps][:longitude] - 0.012, latitude: params[:gps][:latitude] - 0.0004) }
        it 'Успешный запрос' do
          post '/api/v1/step', params: params
          expect(response.status).to eq(201)
          expect(JSON.parse(response.body)).to be_truthy
        end
      end

      context 'Есть вошедшие в границы квадрата' do
        let!(:sign_3) { create(:sign, longitude: params[:gps][:longitude] - 0.00058, latitude: params[:gps][:latitude] - 0.0005) }
        it 'Успешный запрос' do
          post '/api/v1/step', params: params
          expect(response.status).to eq(201)
          expect(JSON.parse(response.body)).to be_truthy
        end
      end
    end

    context 'В границе нет ближайших знаков' do
      context 'Ни одного' do
        xit 'Успешный запрос' do
          post '/api/v1/step', params: params
          expect(response.status).to eq(404)
        end
      end

      context 'Один' do
        let!(:sign_1) { create(:sign, longitude: params[:gps][:longitude] - 0.00032, latitude: params[:gps][:latitude] - 0.0003) }
        xit 'Успешный запрос' do
          post '/api/v1/step', params: params
          expect(response.status).to eq(404)
        end
      end
    end
  end
end
