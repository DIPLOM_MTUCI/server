# frozen_string_literal: true

module Helpers
  module FactoryBotHelper
    def command_darknet(path = Rack::Test::UploadedFile.new('spec/fixtures/files/znk.jpg', 'image/jpg').local_path)
      "cd .. && darknet detect server/storage/yolo/yolo.cfg server/storage/yolo/yolo.weights #{path} && cp predictions.jpg server/tmp/image/predictions.jpg"
    end
  end
end
